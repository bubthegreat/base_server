import logging
import logging.config
import os

from server_base.settings import DEBUG
from server_base.settings import BASE_DIR

DATE_FORMAT = '%b %d, %Y %H:%M:%S%z'
INFO_FORMAT = '%(levelname)s: %(asctime)s - %(process)d.%(module)s.%(funcName)s.%(lineno)d -  %(message)s'
DEBUG_FORMAT = '%(levelname)s: %(asctime)s - %(process)d.%(module)s.%(funcName)s.%(lineno)d - %(message)s'
INFO_FILE = os.path.join(BASE_DIR, 'logs', 'server_base_info.log')
DEBUG_FILE = os.path.join(BASE_DIR, 'logs', 'server_base_debug.log')

# For my purposes, I only want debug to go to the console.  This may change later, but for now
# it's been true enough.


class DebugFilter(logging.Filter):
    """Allows only DEBUG level objects to pass."""

    def filter(self, rec):
        """Filter by logging level number of DEBUG."""
        return rec.levelno == logging.DEBUG


def configure_logger():
    """Dynamically instantiate the logger, unless it is already instantiated."""
    # If we've already got the logger instantiated, we don't want to re-instantiated it
    # like when django re-runs the test server, etc.
    if not logging.Logger.manager.loggerDict.get('server_base'):
        logger = logging.getLogger('server_base')
        # Configure info handler
        info_handler = logging.handlers.TimedRotatingFileHandler(INFO_FILE, when='D', interval=1,
                                                                 backupCount=7)
        info_handler.setLevel(logging.INFO)
        info_formatter = logging.Formatter(fmt=INFO_FORMAT, datefmt=DATE_FORMAT)
        info_handler.setFormatter(info_formatter)

        # Configure debug handlers
        debug_formatter = logging.Formatter(fmt=DEBUG_FORMAT, datefmt=DATE_FORMAT)
        # Console handler
        debug_console_handler = logging.StreamHandler()
        debug_console_handler.setLevel(logging.DEBUG)
        debug_console_handler.addFilter(DebugFilter())
        debug_console_handler.setFormatter(debug_formatter)
        # Log handler
        debug_log_handler = logging.handlers.TimedRotatingFileHandler(DEBUG_FILE, when='D', interval=1,
                                                                      backupCount=7)
        debug_log_handler.setLevel(logging.DEBUG)
        debug_log_handler.addFilter(DebugFilter())
        debug_log_handler.setFormatter(debug_formatter)

        # Add the handlers
        logger.addHandler(info_handler)
        logger.addHandler(debug_console_handler)
        logger.addHandler(debug_log_handler)

        # Check if we're in debug - if so, set debug to true.
        if DEBUG:
            logger.setLevel(logging.DEBUG)
        else:
            # If we're at a logging level higher than debug, get whatever level root is set to.
            logger.setLevel(logging.getLogger().getEffectiveLevel())


configure_logger()
