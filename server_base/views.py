from django.shortcuts import render


def index(request):
    return render(request, 'server_base/index.html')
