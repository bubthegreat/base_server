from fabric.api import env
from fabric.api import run

from fabric.contrib.project import rsync_project


conf = {}

conf['HOST'] = ''  # host URL that you would ssh to. Ex: bubdev.slc.purestorage.com/10.204.116.216
conf['UTIL_USER'] = ''  # username for running commands and logging in.
conf['UTIL_PASSWORD'] = ''  # password for user that will run the commands remotely.  Ex: P@s$w0rd
conf['SUDO_USER'] = ''  # username of sudo priviledged user
conf['SUDO_PASSWORD'] = ''  # password of sudo priviledged user

def host_type():
    run('uname -s')

def rsync_upload():
    """
    Uploads the project with rsync excluding some files and folders.
    """
    excludes = ["*.pyc", "*.pyo", "*.db", ".DS_Store", ".coverage",
                "local_settings.py", "/static", "/.git", "/.hg"]
    local_dir = os.getcwd() + os.sep
    return rsync_project(remote_dir=env.proj_path, local_dir=local_dir,
                         exclude=excludes)
